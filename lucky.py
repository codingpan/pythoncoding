#！ python3
import requests, sys, webbrowser, bs4

print('Starting')

linkAddr = 'https://www.baidu.com/s?wd='+' '.join(sys.argv[1:])
print(linkAddr)

res = requests.get('https://www.baidu.com/s?wd=' + ' '.join(sys.argv[1:]))
print(res.text)
res.raise_for_status()

soup = bs4.BeautifulSoup(res.text,features='html.parser')

linkElems = soup.select('.t a')

print(linkElems)

numOpen = min(5, len(linkElems))

for i in range(numOpen):