import random

capitals = {'Alabama': 'Montgomery', 'Alaska': 'Juneau', 'Arizona': 'Phoenix',
            'Arkansas': 'Little Rock', 'California': 'Sacramento', 'Colorado': 'Denver',
            'Connecticut': 'Hartford', 'Delaware': 'Dover', 'Florida': 'Tallahassee',
            'Georgia': 'Atlanta', 'Hawaii': 'Honolulu', 'Idaho': 'Boise', 'Illinois':
            'Springfield', 'Indiana': 'Indianapolis', 'Iowa': 'Des Moines', 'Kansas':
            'Topeka', 'Kentucky': 'Frankfort', 'Louisiana': 'Baton Rouge', 'Maine':
            'Augusta', 'Maryland': 'Annapolis', 'Massachusetts': 'Boston', 'Michigan':
            'Lansing', 'Minnesota': 'Saint Paul', 'Mississippi': 'Jackson', 'Missouri':
            'Jefferson City', 'Montana': 'Helena', 'Nebraska': 'Lincoln', 'Nevada':
            'Carson City', 'New Hampshire': 'Concord', 'New Jersey': 'Trenton', 'New Mexico': 'Santa Fe', 'New York': 'Albany', 'North Carolina': 'Raleigh',
            'North Dakota': 'Bismarck', 'Ohio': 'Columbus', 'Oklahoma': 'Oklahoma City',
            'Oregon': 'Salem', 'Pennsylvania': 'Harrisburg', 'Rhode Island': 'Providence',
            'South Carolina': 'Columbia', 'South Dakota': 'Pierre', 'Tennessee':
            'Nashville', 'Texas': 'Austin', 'Utah': 'Salt Lake City', 'Vermont':
            'Montpelier', 'Virginia': 'Richmond', 'Washington': 'Olympia', 'West Virginia': 'Charleston', 'Wisconsin': 'Madison', 'Wyoming': 'Cheyenne'}

for quizNum in range(35):
    quizFile = open('D://capticalsquiz%s.txt' % (quizNum+1), 'w')
    answerKeyFile = open('D://capticalquiz_answer%s.txt' % (quizNum+1), 'w')
    #创建问卷的头部提示语句
    quizFile.write('Name:\n\nDate:\n\nPeriod:\n\n')
    quizFile.write(('  '*20)+'State Capitals Quiz (Form %s)' % (quizNum+1))
    quizFile.write('\n\n')

    states = list(capitals.keys())#将州存到list中
    random.shuffle(states)#将顺序打乱

    #创建50个问题
    for questionNum in range(50):
        correctAnswer = capitals[states[questionNum]]#遍历乱序的states对应的首府 存为正确答案
        wrongAnswer = list(capitals.values())#遍历所有的首府
        del wrongAnswer[wrongAnswer.index(correctAnswer)]#将正确的首府去掉
        wrongAnswer = random.sample(wrongAnswer, 3)#随机选择3个值存起来
        answerOptions = wrongAnswer + [correctAnswer]
        random.shuffle(answerOptions)

        quizFile.write('%s. What is the capital of %s?\n'%(questionNum+1, states[questionNum]))

        for i in range(4):
            quizFile.write(' %s. %s\n'%('ABDC'[i], answerOptions[i]))

        quizFile.write('\n')
        answerKeyFile.write('%s. %s\n'%(questionNum+1, 'ABCD'[answerOptions.index(correctAnswer)]))

quizFile.close()
answerKeyFile.close()

    

