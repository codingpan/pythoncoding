#! pyhton3

import webbrowser
import pyperclip
import sys

if len(sys.argv) > 1:
    address = ' '.join(sys.argv[1:])
else:
    # 从剪切板获取数据
    address = pyperclip.paste()

print(sys.argv)

webbrowser.open('https://www.google.com/maps/place/'+address)
