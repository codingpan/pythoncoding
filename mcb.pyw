#! python3
# py.exe mcb.pyw save <keyword> 保存剪切板中的数据
import shelve, pyperclip, sys

mcbShelf = shelve.open('mcb')

# 保存剪切板中的数据
if len(sys.argv) == 3 and sys.argv[1].lower() == 'save':
    mcbShelf[sys.argv[2]]=pyperclip.paste()
# 列出所有的关键字
elif len(sys.argv) == 2:
    if sys.argv[1].lower() == 'list':
        pyperclip.copy(str(list(mcbShelf.keys())))#将关键字都拷贝到剪切板中
    elif sys.argv[1] in mcbShelf:
        pyperclip.copy(mcbShelf[sys.argv[1]])#根据给出的关键字 将对应的内容拷贝到剪切板中

mcbShelf.close()