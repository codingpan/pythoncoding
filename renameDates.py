import shutil
import os
import re

datePattern = re.compile(r'''
    ^(.*?)
    ((0|1)?\d)-
    ((0|1|2|3)?\d)-
    ((19|20)\d\d)
    (.*?)$
''', re.VERBOSE)

for amerFilename in os.listdir('.'):
    print('file='+amerFilename+' time='+str(os.path.getmtime(amerFilename)))
    mo = datePattern.search(amerFilename)

    if mo == None:
        continue

    beforePart = mo.group(1)
    monthPart = mo.group(2)
    dayPart = mo.group(4)
    yearPart = mo.group(6)
    afterPart = mo.group(8)

    euroFilename = beforePart+dayPart+'-'+monthPart+'-'+yearPart+afterPart

    # 获取绝对路径
    absWorkingDir = os.path.abspath('.')
    amerFilename = os.path.join(absWorkingDir, amerFilename)
    euroFilename = os.path.join(absWorkingDir, euroFilename)

    print('Renaming "%s" to "%s" ...' % (amerFilename, euroFilename))
    #shutil.move(amerFilename, euroFilename)
